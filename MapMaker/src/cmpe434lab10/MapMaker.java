package cmpe434lab10;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import javax.swing.JFrame;

import lejos.hardware.BrickFinder;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.geometry.Line;
import lejos.robotics.mapping.LineMap;
import lejos.robotics.navigation.MovePilot;
import lejos.robotics.navigation.Navigator;
import lejos.utility.Delay;

public class MapMaker{

	private static final long serialVersionUID = -2230293867912953496L;
	static EV3 ev3 = (EV3) BrickFinder.getDefault();
	static LineMap map = new LineMap();
	
	static EV3UltrasonicSensor ultrasonicSensor = new EV3UltrasonicSensor(SensorPort.S2);
	
	static MovePilot pilot;
	
	static float getUltrasonicSensorValue() {
		SampleProvider sampleProvider = ultrasonicSensor.getDistanceMode();
		if(sampleProvider.sampleSize() > 0) {
			float [] samples = new float[sampleProvider.sampleSize()];
			sampleProvider.fetchSample(samples, 0);
			return samples[0];
		}
		return -1;		
	}
	
	
	public static void main(String[] args) throws IOException {
		GraphicsLCD graphicsLCD = ev3.getGraphicsLCD();
		EV3LargeRegulatedMotor leftMotor  = new EV3LargeRegulatedMotor(MotorPort.A),
							   rightMotor = new EV3LargeRegulatedMotor(MotorPort.D),
							   cageMotor  = new EV3LargeRegulatedMotor(MotorPort.C);
		
		float leftDiameter  = 5.645f,
			  rightDiameter = 5.655f,
    		  trackWidth    = 14.60f;
    	
		boolean reverse=true;
		//cageMotor.rotate(-182);
		WheeledChassis chassis = new WheeledChassis(
	    		new Wheel[]{
	    				WheeledChassis.modelWheel(leftMotor,leftDiameter).offset(-trackWidth/2).invert(reverse),
	    				WheeledChassis.modelWheel(rightMotor,rightDiameter).offset(trackWidth/2).invert(reverse)}, 
	    		WheeledChassis.TYPE_DIFFERENTIAL);

		

    	MovePilot pilot = new MovePilot(chassis);
    	

		ServerSocket serverSocket = new ServerSocket(1234);
		
		graphicsLCD.clear();
		graphicsLCD.drawString("WhereAMI", graphicsLCD.getWidth()/2, 0, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
		graphicsLCD.drawString("Waiting", graphicsLCD.getWidth()/2, 20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
		graphicsLCD.refresh();
		
		Socket client = serverSocket.accept();
		
		graphicsLCD.clear();
		graphicsLCD.drawString("WhereAMI", graphicsLCD.getWidth()/2, 0, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
		graphicsLCD.drawString("Connected", graphicsLCD.getWidth()/2, 20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
		graphicsLCD.refresh();
        
		OutputStream outputStream = client.getOutputStream();
		DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
	//	Navigator navigator=new Navigator(pilot);
		//float[] way={25f,0f,25f,25f,0f,25f,0f,0f};
		float x = 200;
		float y = 300;
		float sx=x;
		float sy=y;
		float[] dirs = {-10f,10f,10f,-10f};
		for(int i=0; i<4; i++){
			for(int j=0; j<10; j++){
				if(i%2==0){
					y += dirs[i];
					sy=y;
					sx=x+getUltrasonicSensorValue();
				}
				else{
					x += dirs[i];
					sy=y+getUltrasonicSensorValue();
				}
				pilot.travel(10,false);
				dataOutputStream.writeFloat(sx);
				dataOutputStream.flush();
				dataOutputStream.writeFloat(sy);
				dataOutputStream.flush();
				Delay.msDelay(500);
			}
			pilot.rotate(-90,true);
		}
	
		dataOutputStream.close();
		serverSocket.close();
		
	}
	
}
