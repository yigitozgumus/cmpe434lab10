
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;

public class MapDrawer extends JFrame {

	private static final long serialVersionUID = 2280874288993963333L;

	static InputStream inputStream;
	static DataInputStream dataInputStream;
	static ArrayList<Line2D> map =new ArrayList<>();
	public MapDrawer() {
		super("CmpE 434");
		setSize( 500, 500 );
		setVisible( true );

		

	}

	public static void main(String[] args) throws Exception	{

		float x=0,y=0;

		MapDrawer monitor = new MapDrawer();

		monitor.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

		String ip = "192.168.43.218";

		@SuppressWarnings("resource")
		Socket socket = new Socket(ip,1234);
		System.out.println("Connected!");

		inputStream = socket.getInputStream();
		dataInputStream = new DataInputStream(inputStream);

		while( true ){
			float xPrev=x;
			float yPrev=y;
			x = dataInputStream.readFloat();
			y = dataInputStream.readFloat();
			System.out.println(x + " " + y );
			map.add(new Line2D.Float(x,y,xPrev,yPrev));
			monitor.repaint();
		}
	}

	public void paint( Graphics g ) {
		super.paint( g );
		displayMap( map, g);
	}

	public void displayMap( ArrayList<Line2D> map, Graphics g ){
		Graphics2D g2 = ( Graphics2D ) g;
		g2.setPaint( Color.BLUE );
		g2.setStroke( new BasicStroke( 5.0f ));

		for ( int i = 1; i < map.size(); i++ ){
			g2.draw(map.get(i));
		}

	}

}